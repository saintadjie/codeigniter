<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\TodolistModel;

class TodolistController extends BaseController
{
    protected $todolist;

    function __construct(){
        $this->todolist = new todolistModel();
    }

    public function index()
    {
        $data['todolist'] = $this->todolist->findAll();
        
        return view('todolist/index', $data);
    }

    public function add()
    {
        
        return view('todolist/tambah');
    }

    public function store()
    {
        if (!$this->validate(
            [
                'nama_kegiatan'=>[
                    'rules' => 'required',
                    'errors' => '{field} harus diisi'
                ],
                'tanggal'=>[
                    'rules' => 'required',
                    'errors' => [
                        'required' => '{field} harus diisi',
                        'valid_email' => 'Email harus valid'
                    ]
                ]
            ]
        )){
            session()->setFlashdata('error', $this->validator->listErrors());
            return redirect()->back()->withInput();
        }
        $this->todolist->insert([
            'nama_kegiatan' => $this->request->getVar('nama_kegiatan'),
            'tanggal' => $this->request->getVar('tanggal'),
            'status' => "BELUM DIKERJAKAN",
        ]);
        session()->setFlashdata('message', "Kegiatan berhasil ditambah");
        return redirect()->to('/');
    }


    public function delete($id){

        $dataTodolist = $this->todolist->find($id); // fungsi delete ke database
        if(empty($dataTodolist)){
            throw new \CodeIgniter\Exceptions\PageNotFoundException('Error');
        }
        $this->todolist->delete($id);
        session()->setFlashdata('message', "Kegiatan berhasil dihapus");
        return redirect()->to('/');
    }

    public function edit($id)
    { {
            $dataTodolist = $this->todolist->find($id);
            if (empty($dataTodolist)) {
                throw new \CodeIgniter\Exceptions\PageNotFoundException('Data Todo List Tidak ditemukan !');
            }
            $data['todolist'] = $dataTodolist;
            return view('/todolist/edit', $data);
        }
    }

    public function update($id)
    {
        if (!$this->validate(
            [
                'nama_kegiatan'=>[
                    'rules' => 'required',
                    'errors' => '{field} harus diisi'
                ],
                'tanggal'=>[
                    'rules' => 'required',
                    'errors' => [
                        'required' => '{field} harus diisi',
                        'valid_email' => 'Email harus valid'
                    ]
                ]
            ]
        )) {
            session()->setFlashdata('error', $this->validator->listErrors());
            return redirect()->back();
        }

        $this->todolist->update($id, [
            'nama_kegiatan' => $this->request->getVar('nama_kegiatan'),
            'tanggal' => $this->request->getVar('tanggal'),
            'status' => $this->request->getVar('status'),
        ]);
        session()->setFlashdata('message', 'Kegiatan berhasil diubah');
        return redirect()->to('/');
    }

    public function change($id)
    {

        $this->todolist->update($id, [
            'status' => $this->request->getVar('status'),
        ]);
    }
}
