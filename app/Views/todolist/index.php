<?php 
    $this->extend('layouts/template') 
?>

<?php 
    $this->section('content') 
?>

<div class="container">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
        	<?php if (!empty(session()->getFlashdata('message'))) : ?>
				<div class="alert alert-dismissible alert-success">
				  	<button type="button" class="btn-close" data-bs-dismiss="alert"></button>
				  	<strong>Well done!</strong> You successfully read <a href="#" class="alert-link">this important alert message</a>.
				</div>
			<?php endif; ?>
            <div class="card">
                <div class="card-header bg-dark">
                    <h4></h4>
                    <div class="card-header-action">
                        <a href="<?php echo base_url('/todolist/tambah') ?>" class="btn btn-sm btn-success" style="float:right;">Tambah Data</a>
                    </div>
                </div>
                <div class="card-body">

                    <div class="table-responsive">
				        <table id="todolist" class="display" style="width:100%">
							<thead>
								<tr>
									<th>Kegiatan</th>
									<th>Tanggal</th>
									<th>Status</th>
									<th>Ubah Status</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($todolist as $value) { ?>

									<tr id="<?php echo $value->id; ?>">
										<td><?php echo $value->nama_kegiatan; ?></td>
										<td><?php echo $value->tanggal; ?></td>
										<td>
									        <?php if($value->status === 'SELESAI'): ?>
									        <span class="badge rounded-pill bg-primary"><?= $value->status; ?></span>
									        <?php else: ?>
									        <span class="badge rounded-pill bg-warning"><?= $value->status; ?></span>
									        <?php endif ?>
									    </td>
									    <td>
									    	<div class="form-check form-switch">
									    		<?php if($value->status === 'SELESAI'): ?>
										        <input class="form-check-input ubah btn_ubah_<?= $value->id; ?>" type="checkbox" id="btn_belum" name="ubah_status" value="SELESAI" data-kodeid="<?php echo $value->id; ?>" checked="">
										        <?php else: ?>
										        <input class="form-check-input ubah btn_ubah_<?= $value->id; ?>" type="checkbox" id="btn_selesai" name="ubah_status" value="BELUM DIKERJAKAN" data-kodeid="<?php echo $value->id; ?>">
										        <?php endif ?>
										    </div>
									    </td>
										<td><a href="<?php echo base_url("/todolist/edit/$value->id") ?>" class="btn btn-sm btn-success">Edit</a> &nbsp; <a href="<?= base_url("/todolist/hapus/$value->id") ?>" class="btn btn-sm btn-danger">Delete</a></td>
									</tr>

								<?php } ?>	
							</tbody>
						</table>
				    </div>
                </div>
            </div>
        </div>         
    </div>
</div>
<script type="text/javascript">
	var url_home   = "<?= base_url('/') ?>"
    var url_change   = "<?= base_url('todolist/change/') ?>"
</script>

<script type="text/javascript">
    $('#todolist tbody').on('click', '.ubah', function(){

        var id = $(this).closest('tr').attr('id');
        var kodeid = $(this).val();
        if(kodeid == "SELESAI"){
        	var kodeid2 = "BELUM DIKERJAKAN";
        }else{
        	var kodeid2 = "SELESAI";
        }

        $.ajax({
            type: "POST",
            url: url_change + '/' + id,
	        data: {
                status       : kodeid2,
            },
            success: function(data) {
                alert('Status berhasil diubah');
                    
            }
        }).then(function(){
            location.href = url_home
        });
;

    })

</script>
<script type="text/javascript">
	$(document).ready(function () {
		// var indexColumn = $("#todolist").find('tr')[0].cells.length - 1;
		// console.log(indexColumn);
	    $('#todolist').DataTable({
	    	"language": {
                "emptyTable":     "Tidak ada data yang tersedia",
                "info":           "Menampilkan _START_ hingga _END_ dari _TOTAL_ data",
                "infoEmpty":      "Menampilkan 0 hingga 0 dari 0 data",
                "infoFiltered":   "(tersaring dari _MAX_ total data)",
                "lengthMenu":     "Tampilkan _MENU_ data",
                "search":         "Pencarian:",
                "zeroRecords":    "Pencarian tidak ditemukan",
                "paginate": {
                    "first":      "Awal",
                    "last":       "Akhir",
                    "next":       "▶",
                    "previous":   "◀"
                },
            },
            "lengthMenu"  : [[10, 25, 50, -1], [10, 25, 50, "Semua"]],
	    	autoWidth:false,
	    	order: [
	    		0, "asc"
	    	],
	    });
	});
</script>

<?php 
    $this->endSection() 
?>