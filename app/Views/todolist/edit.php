<?php 
    $this->extend('layouts/template') 
?>

<?php 
    $this->section('content') 
?>

<div class="container">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
        	<?php if (!empty(session()->getFlashdata('error'))) : ?>
			    <div class="alert alert-dismissible alert-danger">
				  	<button type="button" class="btn-close" data-bs-dismiss="alert"></button>
				  	<strong>Oh snap!</strong> <a href="#" class="alert-link">Change a few things up</a> and try submitting again.
				</div>
			<?php endif; ?>
			<?php if (!empty(session()->getFlashdata('message'))) : ?>
				<div class="alert alert-dismissible alert-success">
				  	<button type="button" class="btn-close" data-bs-dismiss="alert"></button>
				  	<strong>Well done!</strong> You successfully read <a href="#" class="alert-link">this important alert message</a>.
				</div>
			<?php endif; ?>
            <div class="card">
                <div class="card-header bg-dark">
                    <h4></h4>
                    <div class="card-header-action">
                        <a href="<?php echo base_url('/') ?>" class="btn btn-sm btn-danger"  style="float:right;">Kembali</a>
                    </div>
                </div>
                <div class="card-body">
                    <form method="POST" action="<?= base_url('todolist/update/'.$todolist->id) ?>">
                    	<?= csrf_field(); ?>
					  	<fieldset>
					    	<div class="form-group">
						      	<label for="nama_kegiatan" class="form-label mt-4">Kegiatan</label>
						      	<input type="text" class="form-control" id="nama_kegiatan" name="nama_kegiatan" placeholder="Masukkan Nama Kegiatan" value="<?= $todolist->nama_kegiatan; ?>">
						    </div>
				      		<div class="form-group">
						      	<label for="tanggal" class="form-label mt-4">Tanggal</label>
						      	<input type="text" class="form-control datepicker" id="tanggal" name="tanggal" aria-describedby="tanggalHelp" placeholder="Masukkan Tanggal Kegiatan" value="<?= $todolist->tanggal; ?>">
						    </div>
						    <fieldset class="form-group">
						      	<label for="status" class="form-label mt-4">Status</label>
						      	<div class="form-check">
							        <input class="form-check-input" type="radio" name="status" id="status1" value="BELUM DIKERJAKAN" <?php echo ($todolist->status == 'BELUM DIKERJAKAN' ? ' checked' : ''); ?>>
							        <label class="form-check-label" for="status1">
							          	BELUM DIKERJAKAN
							        </label>
						      	</div>
						      	<div class="form-check">
							        <input class="form-check-input" type="radio" name="status" id="status2" value="SELESAI" <?php echo ($todolist->status == 'SELESAI' ? ' checked' : ''); ?>>
							        <label class="form-check-label" for="status2">
							          	SELESAI
							        </label>
						      	</div>
						    </fieldset>
						    <br/>
						    <input type="submit" value="SIMPAN" class="btn btn-primary">
				      	</fieldset>
					</form>
                </div>
            </div>
        </div>         
    </div>
</div>
<script type="text/javascript">
	$('.datepicker').datepicker({
	    format: 'yyyy-mm-dd',
	    autoclose: true,
	});
</script>

<?php 
    $this->endSection() 
?>
