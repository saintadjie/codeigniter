<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Todolist extends Migration
{
    public function up()
    {
        $this->forge->addField(
            [
                'id' => [
                    'type' => 'INT',
                    'auto_increment' => true,
                    'constraint' => '11',
                ],
                'nama_kegiatan' => [
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                ],
                'tanggal' => [
                    'type' => 'DATE',
                ],
                'status' => [
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                ],
                'created_at' => [
                    'type' => 'DATETIME',
                    'null' => true,
                ],
                'updated_at' => [
                    'type' => 'DATETIME',
                    'null' => true,
                ],
            ],

        );
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('todolist');
    }

    public function down()
    {
        $this->forge->dropTable('todolist');
    }
}
