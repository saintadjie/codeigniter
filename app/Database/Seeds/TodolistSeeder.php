<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;
use CodeIgniter\I18n\Time;

class TodolistSeeder extends Seeder
{
    public function run()
    {
        for($i=0; $i<100; $i++) {
          $faker = \Faker\Factory::create();
          $data = [
            'nama_kegiatan' => $faker->bs,
            'tanggal'    => $faker->date($format = 'Y-m-d', $min = 'now', $max = '+1 years'),
            'status'    => $faker->randomElement($array = array ('BELUM DIKERJAKAN','SELESAI')),
            'created_at' => Time::now()
          ];
          $this->db->table('todolist')->insert($data);
        }
    }
}
