-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 06, 2022 at 02:35 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `codeigniter`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `version` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL,
  `group` varchar(255) NOT NULL,
  `namespace` varchar(255) NOT NULL,
  `time` int(11) NOT NULL,
  `batch` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `version`, `class`, `group`, `namespace`, `time`, `batch`) VALUES
(2, '2022-08-03-070014', 'App\\Database\\Migrations\\Peserta', 'default', 'App', 1659512853, 1);

-- --------------------------------------------------------

--
-- Table structure for table `peserta`
--

CREATE TABLE `peserta` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `peserta`
--

INSERT INTO `peserta` (`id`, `name`, `email`, `created_at`, `updated_at`) VALUES
(1, 'Arielle Ratke', 'gerardo.hammes@gmail.com', '2022-08-03 02:47:49', NULL),
(2, 'Fiona Goodwin', 'pgusikowski@hotmail.com', '2022-08-03 02:47:49', NULL),
(3, 'Fidel Okuneva PhD', 'annamae62@hotmail.com', '2022-08-03 02:47:49', NULL),
(4, 'Lily Streich', 'vkris@hotmail.com', '2022-08-03 02:47:49', NULL),
(5, 'Giovanni Hane', 'bauch.rylee@thompson.com', '2022-08-03 02:47:49', NULL),
(6, 'Miss Beatrice Lindgren DVM', 'jennifer.torp@hotmail.com', '2022-08-03 02:47:49', NULL),
(7, 'Ms. Alysson Hauck', 'angela38@tremblay.info', '2022-08-03 02:47:49', NULL),
(8, 'Shanie Greenfelder', 'rafaela19@schmitt.com', '2022-08-03 02:47:49', NULL),
(9, 'Dr. Eldridge Smith', 'dean.miller@stamm.org', '2022-08-03 02:47:49', NULL),
(10, 'Julia Kirlin', 'feil.asha@kulas.biz', '2022-08-03 02:47:49', NULL),
(11, 'Prof. Darron Kris', 'rene.doyle@yahoo.com', '2022-08-03 02:47:49', NULL),
(12, 'Ambrose Greenholt', 'fokon@bosco.info', '2022-08-03 02:47:49', NULL),
(13, 'Mr. Roel Franecki', 'gisselle70@hotmail.com', '2022-08-03 02:47:49', NULL),
(14, 'Cordia Roberts', 'xtowne@kuvalis.com', '2022-08-03 02:47:49', NULL),
(15, 'Jarvis Gaylord I', 'tomas.wehner@yahoo.com', '2022-08-03 02:47:49', NULL),
(16, 'Brenda Witting', 'wiegand.howell@gmail.com', '2022-08-03 02:47:49', NULL),
(17, 'Chelsea Kulas', 'rice.dock@howe.com', '2022-08-03 02:47:49', NULL),
(18, 'Marlee Schaden', 'yoconner@hotmail.com', '2022-08-03 02:47:49', NULL),
(19, 'Felipa Lueilwitz', 'xpowlowski@stoltenberg.biz', '2022-08-03 02:47:49', NULL),
(20, 'Raegan Kub Sr.', 'cyrus82@moore.com', '2022-08-03 02:47:49', NULL),
(21, 'Prof. Floy Deckow', 'donnell.reynolds@hotmail.com', '2022-08-03 02:47:49', NULL),
(22, 'Kayley Goldner', 'effertz.dedrick@gmail.com', '2022-08-03 02:47:49', NULL),
(23, 'Prof. Ian Hayes', 'delta.mcglynn@gmail.com', '2022-08-03 02:47:49', NULL),
(24, 'Dario Hermann', 'euna85@yahoo.com', '2022-08-03 02:47:49', NULL),
(25, 'Ms. Emie Wehner DVM', 'sanford.bradley@flatley.info', '2022-08-03 02:47:49', NULL),
(26, 'Edwardo Roberts', 'pattie.graham@bashirian.info', '2022-08-03 02:47:49', NULL),
(27, 'Aaliyah Feil', 'selmer.hickle@yahoo.com', '2022-08-03 02:47:49', NULL),
(28, 'Dr. Lera Bosco DVM', 'felipe98@yahoo.com', '2022-08-03 02:47:49', NULL),
(29, 'Juston Reinger', 'judson72@rolfson.info', '2022-08-03 02:47:49', NULL),
(30, 'Jaclyn Bailey', 'jettie.batz@hotmail.com', '2022-08-03 02:47:49', NULL),
(31, 'Carolina Wuckert', 'myrtice24@bartoletti.com', '2022-08-03 02:47:49', NULL),
(32, 'Justus Waters', 'lou46@pacocha.com', '2022-08-03 02:47:49', NULL),
(33, 'Sigmund Veum', 'daphney.fadel@windler.net', '2022-08-03 02:47:49', NULL),
(34, 'Prof. Nicholas Murray DDS', 'leone.okon@hotmail.com', '2022-08-03 02:47:49', NULL),
(35, 'Mr. Giuseppe Russel', 'percival20@cremin.biz', '2022-08-03 02:47:49', NULL),
(36, 'Coralie Sipes', 'lucile.roob@gmail.com', '2022-08-03 02:47:49', NULL),
(37, 'Prof. Guido Ebert Sr.', 'katrine.schroeder@yahoo.com', '2022-08-03 02:47:49', NULL),
(38, 'Jeromy Hagenes I', 'ogreenfelder@gmail.com', '2022-08-03 02:47:49', NULL),
(39, 'Darrel Kovacek', 'ciara39@gmail.com', '2022-08-03 02:47:49', NULL),
(40, 'Mrs. Lola Luettgen III', 'laurianne.hamill@hotmail.com', '2022-08-03 02:47:49', NULL),
(41, 'Cale Lehner', 'durgan.troy@dietrich.com', '2022-08-03 02:47:49', NULL),
(42, 'Ollie Romaguera', 'kurtis.fisher@yahoo.com', '2022-08-03 02:47:49', NULL),
(43, 'Gretchen Deckow', 'dmurphy@hotmail.com', '2022-08-03 02:47:49', NULL),
(44, 'Clementine Schimmel', 'hgulgowski@gmail.com', '2022-08-03 02:47:49', NULL),
(45, 'Jedediah Ullrich', 'beulah40@durgan.org', '2022-08-03 02:47:49', NULL),
(46, 'Cara Watsica DVM', 'angus80@little.info', '2022-08-03 02:47:49', NULL),
(47, 'Heloise Marvin', 'tmarks@gmail.com', '2022-08-03 02:47:49', NULL),
(48, 'Hortense Dicki', 'omoore@hotmail.com', '2022-08-03 02:47:49', NULL),
(49, 'Alberta Hermiston', 'greichel@yahoo.com', '2022-08-03 02:47:49', NULL),
(50, 'Cortney Farrell', 'fcrooks@doyle.org', '2022-08-03 02:47:49', NULL),
(51, 'Mrs. Maryjane Legros IV', 'collins.eugenia@hoeger.com', '2022-08-03 02:47:49', NULL),
(52, 'Dell Wehner', 'cletus.gutmann@gmail.com', '2022-08-03 02:47:49', NULL),
(53, 'Mrs. Dixie Brakus DDS', 'jaskolski.junius@west.com', '2022-08-03 02:47:49', NULL),
(54, 'Laurine Schinner', 'ardith.schuster@hotmail.com', '2022-08-03 02:47:49', NULL),
(55, 'Mrs. Allene Hilpert', 'ledner.johnathan@dach.com', '2022-08-03 02:47:49', NULL),
(56, 'Vanessa Harris', 'vlakin@braun.com', '2022-08-03 02:47:49', NULL),
(57, 'Tiana Heaney', 'ara92@fadel.com', '2022-08-03 02:47:49', NULL),
(58, 'Ms. Maegan Hermann', 'earl84@collier.com', '2022-08-03 02:47:49', NULL),
(59, 'Lacey Murazik', 'hammes.jodie@walter.com', '2022-08-03 02:47:49', NULL),
(60, 'Prof. Gracie Jacobs III', 'pasquale.jacobson@yahoo.com', '2022-08-03 02:47:49', NULL),
(61, 'Vilma Heller', 'lgutkowski@mertz.com', '2022-08-03 02:47:49', NULL),
(62, 'Toby Jaskolski Sr.', 'lila.dietrich@ritchie.com', '2022-08-03 02:47:49', NULL),
(63, 'Dorothea Schaefer', 'carlo.kling@crona.com', '2022-08-03 02:47:49', NULL),
(64, 'Mrs. Asha Dach', 'mann.martine@fadel.com', '2022-08-03 02:47:49', NULL),
(65, 'Terrell Greenholt', 'pacocha.hester@gmail.com', '2022-08-03 02:47:49', NULL),
(66, 'Mr. Stefan Goodwin', 'larkin.pete@gmail.com', '2022-08-03 02:47:49', NULL),
(67, 'Kevon Hyatt', 'murl.weissnat@yahoo.com', '2022-08-03 02:47:49', NULL),
(68, 'Kayli Beer DDS', 'haylee58@yahoo.com', '2022-08-03 02:47:49', NULL),
(69, 'Randall Bartoletti IV', 'dmetz@hotmail.com', '2022-08-03 02:47:49', NULL),
(70, 'Holly Murphy DVM', 'tatum.schuster@batz.com', '2022-08-03 02:47:49', NULL),
(71, 'Mr. Garrick Schamberger', 'durgan.camilla@white.biz', '2022-08-03 02:47:49', NULL),
(72, 'Cristian Reynolds', 'britney.pouros@hoeger.com', '2022-08-03 02:47:49', NULL),
(73, 'Rosalia Bosco', 'kenya.erdman@yahoo.com', '2022-08-03 02:47:49', NULL),
(74, 'Prof. Alva Boyle', 'ycronin@ryan.com', '2022-08-03 02:47:49', NULL),
(75, 'Muhammad Lueilwitz', 'borer.harmon@zieme.com', '2022-08-03 02:47:49', NULL),
(76, 'Milton Wisozk', 'afarrell@kuhn.com', '2022-08-03 02:47:49', NULL),
(77, 'Miss Evie Frami', 'hassie36@torphy.com', '2022-08-03 02:47:49', NULL),
(78, 'Baby VonRueden', 'fahey.edgar@hotmail.com', '2022-08-03 02:47:49', NULL),
(79, 'Alfred Deckow Jr.', 'huel.trudie@hotmail.com', '2022-08-03 02:47:49', NULL),
(80, 'Adam Paucek', 'lauryn05@haley.com', '2022-08-03 02:47:49', NULL),
(81, 'Prof. Jayda Stanton Jr.', 'kling.art@brown.net', '2022-08-03 02:47:49', NULL),
(82, 'Donavon Rau', 'kayla.conn@yahoo.com', '2022-08-03 02:47:49', NULL),
(83, 'Howard Mertz', 'stoltenberg.stefanie@hand.biz', '2022-08-03 02:47:49', NULL),
(84, 'Dr. Lucile Torp', 'dbergnaum@marvin.net', '2022-08-03 02:47:49', NULL),
(85, 'Miss Natalie Smitham DDS', 'stehr.darius@yahoo.com', '2022-08-03 02:47:49', NULL),
(86, 'Prof. Lexie Heller Sr.', 'elakin@pollich.com', '2022-08-03 02:47:49', NULL),
(87, 'Glen Greenholt', 'cstanton@gmail.com', '2022-08-03 02:47:49', NULL),
(88, 'Cleta Friesen I', 'powlowski.kyla@walter.biz', '2022-08-03 02:47:49', NULL),
(89, 'Perry Bins', 'reva96@lehner.com', '2022-08-03 02:47:49', NULL),
(90, 'Irving Thiel', 'bauch.ismael@schultz.com', '2022-08-03 02:47:49', NULL),
(91, 'Oma Lemke', 'metz.adelle@hotmail.com', '2022-08-03 02:47:49', NULL),
(92, 'Viva Kassulke II', 'clovis.haag@hotmail.com', '2022-08-03 02:47:49', NULL),
(93, 'Caesar Ledner', 'nina72@gmail.com', '2022-08-03 02:47:49', NULL),
(94, 'Adrienne Huels', 'eliseo18@hodkiewicz.com', '2022-08-03 02:47:49', NULL),
(95, 'Maya Homenick', 'ziemann.clementina@gmail.com', '2022-08-03 02:47:49', NULL),
(96, 'Aaron Stracke', 'garfield50@kshlerin.com', '2022-08-03 02:47:49', NULL),
(97, 'Devin Walter', 'oconnell.marianna@roob.com', '2022-08-03 02:47:49', NULL),
(98, 'Lempi Jast', 'cummings.curtis@koss.info', '2022-08-03 02:47:49', NULL),
(99, 'Prof. Vella Crona DVM', 'abbott.bradley@weissnat.com', '2022-08-03 02:47:49', NULL),
(100, 'Ms. Mya Nitzsche MD', 'pansy83@schroeder.com', '2022-08-03 02:47:49', NULL),
(101, 'Adjie Kr', 'saintadjie@gmail.com', '2022-08-03 05:33:23', '2022-08-03 05:33:23'),
(102, 'Lidiya', 'Lidiya@imigrasi.go.id', '2022-08-03 05:34:00', '2022-08-03 05:34:00'),
(103, 'Hana', 'hana@gmail.com', '2022-08-03 05:34:57', '2022-08-03 05:34:57'),
(105, 'Demo', 'demo@gmail.com', '2022-08-03 05:39:10', '2022-08-03 05:39:10'),
(106, 'ddad', 'dad@dada.dd', '2022-08-03 05:40:19', '2022-08-03 05:40:19'),
(111, 'xxxxxxxx', '', '2022-08-03 05:50:34', '2022-08-03 05:50:34'),
(113, 'zzzz', '', '2022-08-03 05:54:41', '2022-08-03 05:54:41'),
(115, 'xcxc', 'zxxz@dada.dad', '2022-08-03 05:56:37', '2022-08-03 05:56:37'),
(116, 'dsadsa', 'safdas@dsad.dasdas', '2022-08-03 05:58:15', '2022-08-03 05:58:15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `peserta`
--
ALTER TABLE `peserta`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `peserta`
--
ALTER TABLE `peserta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=118;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
